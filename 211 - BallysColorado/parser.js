// JavaScript Document
// MLA - money line away = away team
// MLH - mnoney line home = home team
// PSA - point spread away = second col
// PSH - point spread home = second col
// TLO - Total over
// TLU - total under - third col use price in this as bracketed number
// <takeBetState>Open</takeBetState> = open show, hide if closed - <status>1</status> if status 0 hide.


var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
if (this.readyState == 4 && this.status == 200) {
  myFunction(this);
}
};
	
xmlhttp.open("GET", "https://co.ballybet.com/sbk/datafeed/eventfeed.xml?action=getEventsByCategoryId&categoryId=204&mode=0&brandId=211", true);
xmlhttp.send();

function myFunction(xml) {
  var i;
  var xmlDoc = xml.responseXML;
  var table="";
  var x = xmlDoc.getElementsByTagName("event");
  for (i = 0; i <x.length; i++) {
    table += "<article class='innerContainer carousel-cell'><table class='table table-bordered tableSet'><tr><td rowspan='2' class='w12 larger'>" +
    x[i].getElementsByTagName("postTimeDateString")[0].childNodes[0].nodeValue + "<br><strong>" + x[i].getElementsByTagName("postTimeTimeString")[0].childNodes[0].nodeValue + "</strong></td><td class='text-center w5'>" +
    x[i].getElementsByTagName("awayTeamRotationNumber")[0].childNodes[0].nodeValue + "</td><td class='w40'><h2>" +
    x[i].getElementsByTagName("awayTeamName")[0].childNodes[0].nodeValue  + "</h2><small>" + x[i].getElementsByTagName("description2")[0].childNodes[0].nodeValue +  "</small></td><td class='w20'><button type='button' class='btn btn-primary btn-md btn-block'>" + x[i].getElementsByTagName("price")[5].childNodes[0].nodeValue + "</button></td><td class='w20'><button type='button' class='btn btn-primary btn-md btn-block'>" +
    x[i].getElementsByTagName("points")[3].childNodes[0].nodeValue + " <span class='badge bg-secondary'>" + x[i].getElementsByTagName("price")[3].childNodes[0].nodeValue + "</span></button></td><td class='w20'><button type='button' class='btn btn-primary btn-md btn-block'>" +
    x[i].getElementsByTagName("points")[1].childNodes[0].nodeValue + " <span class='badge bg-secondary'>" + x[i].getElementsByTagName("price")[0].childNodes[0].nodeValue + "</span></button></td>" +
   
	"</tr>" +
	"<tr><td class='w5'>" + x[i].getElementsByTagName("homeTeamRotationNumber")[0].childNodes[0].nodeValue + "</td><td class='w40'><h2>" +
    x[i].getElementsByTagName("homeTeamName")[0].childNodes[0].nodeValue  + "</h2><small>" + x[i].getElementsByTagName("description1")[0].childNodes[0].nodeValue +  "</small></td><td class='w20'><button type='button' class='btn btn-primary btn-md btn-block'>" + x[i].getElementsByTagName("price")[2].childNodes[0].nodeValue + "</button></td><td class='w20'><button type='button' class='btn btn-primary btn-md btn-block'>" +
    x[i].getElementsByTagName("points")[0].childNodes[0].nodeValue + " <span class='badge bg-secondary'>" + x[i].getElementsByTagName("price")[0].childNodes[0].nodeValue + "</span></button></td><td class='w20'><button type='button' class='btn btn-primary btn-md btn-block'>" +
    x[i].getElementsByTagName("points")[4].childNodes[0].nodeValue + " <span class='badge bg-secondary'>" + x[i].getElementsByTagName("price")[4].childNodes[0].nodeValue + "</span></button></td>" +
    "</td></tr></table></article>";
  }
  document.getElementById("eventTable").innerHTML = table;
}
